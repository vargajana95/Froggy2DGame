package junittest;

import java.awt.Point;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import game.Screen;
import game.player.Player;

public class PlayerTest {
	private Player player;
	
	@Before
	public void setUp()
	{
		player = new Player("/player.png", new Point(0, 0));
	}
	
	@Test
	public void reSpawnTest()
	{
		Point reSpawnPoint = new Point(360, 576);
		
		player.reSpawn();
		player.update();
		Assert.assertEquals(player.getPosition(), reSpawnPoint);
	}
	
	@Test
	public void killTest()
	{
		//A Player-nek 3 �lete van
		//3 szor meg�lj�k
		player.update();
		player.kill();
		player.reSpawn();

		Assert.assertEquals(2, player.getLifes());
		Assert.assertEquals(false, player.isDead());
		player.kill();
		player.reSpawn();
		player.kill();
		
		Assert.assertEquals(0, player.getLifes());
		Assert.assertEquals(true, player.isDead());
	}
	
	@Test 
	public void isOutOfScreenTest()
	{
		player.setPosition(Screen.getSize().width+1, 0);
		Assert.assertEquals(true, player.isOutOfScreen());

		
		player.setPosition(Screen.getSize().width-1, 0);
		player.update();
		Assert.assertEquals(false, player.isOutOfScreen());
	}
	
}
