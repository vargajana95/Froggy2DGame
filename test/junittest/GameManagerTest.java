package junittest;

import java.awt.Point;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import game.GameManager;
import game.entities.pickups.LifePickup;
import game.entities.pickups.Pickup;

public class GameManagerTest {
	private GameManager gameManager;
	
	@Before
	public void setUp()
	{
		gameManager = new GameManager();
		gameManager.newGame();
	}
	
	@Test
	public void WinTest()
	{
		for(int i = 0; i < 5; i++)
			gameManager.getPlayer().increaseOccupiedHomeBoxes();
		
		gameManager.checkForWin(gameManager.getPlayer());
		
		Assert.assertEquals(2, gameManager.getLevel());
	}
	
	@Test
	public void GameManagerSpawnEntityTest()
	{
		Pickup p = new LifePickup(new Point(0, 0));
		gameManager.spawnEntity(p);
		gameManager.update();
		
		Assert.assertEquals(true, gameManager.getEntities().contains(p));
	}
	
	@Test
	public void GameManagerRemoveEntityTest()
	{		
		Pickup p = new LifePickup(new Point(0, 0));
		
		gameManager.removeEntity(p);
		gameManager.update();
		
		Assert.assertEquals(false, gameManager.getEntities().contains(p));
	}
}
