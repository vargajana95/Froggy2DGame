package junittest;


import java.awt.Point;
import java.awt.Rectangle;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import game.entities.movers.Car;
import game.entities.movers.Mover;
import game.entities.movers.Platform;
import game.entities.pickups.InvinciblePickup;
import game.entities.pickups.LifePickup;
import game.entities.pickups.Pickup;
import game.player.Player;
public class OtherTest {

private Player player;
	
	@Before
	public void setUp()
	{
		player = new Player("/player.png", new Point(0, 0));
	}
	
	@Test
	public void invinciblePickupCollisionTest()
	{
		Pickup p = new InvinciblePickup(new Point(0, 0));
		
		p.onCollision(player);
		
		Assert.assertEquals(true, player.isInvincible());

	}
	
	@Test
	public void lifePickupCollisionTest()
	{
		Pickup p = new LifePickup(new Point(0, 0));
		
		p.onCollision(player);
		
		Assert.assertEquals(4, player.getLifes());
		
	}
	
	@Test
	public void plaformCollisionTest()
	{
		Mover p = new Platform("/TrunkSheet.png", new Point(-576, 64), new Rectangle(-576, 64, 128, 64), Mover.Direction.RIGHT, 10);
		
		p.onCollision(player);
		
		Assert.assertNotEquals(null, player.isOnPlatform());
		
	}
	
	@Test
	public void carCollisionTest()
	{
		Mover car = new Car("/CarSheet.png", new Point(65, 512), Mover.Direction.RIGHT, 
				10, 0);
		
		car.onCollision(player);
		
		Assert.assertNotEquals(true, player.isRespawning());
	}
	
	
}
