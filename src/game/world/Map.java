package game.world;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import game.graphics.TileSheet;
import game.tiles.NormalTile;
import game.tiles.Tile;
import game.tiles.WaterTile;

public class Map {
	//TODO read this from the map file
	private int height = 13;
	private int width = 15;
	
	private transient TileSheet tileSheet;
	private Tile tiles[][];
	
	public Map()
	{
		tileSheet = new TileSheet("/MapSheet.png");
		tiles = new Tile[height][width];
	}
	/**
	 * Minden tile-t kirajzol� f�ggv�ny.
	 * @param screen a k�perny�.
	 */
	public void draw(Graphics screen)
	{
		for(int i = 0; i < height; i++)
		{
			for(int j = 0; j < width; j++)
			{
				tiles[i][j].drawTile(screen, j*tileSheet.getTileHeight(), i*tileSheet.getTileWidth());
			}
		}
	}
	
	public void update()
	{
		
	}
	/**
	 * A param�tereknek megfelel� tile-al t�r vissza
	 * @param col az oszlop
	 * @param row a sor
	 * @return a tile
	 */
	public Tile getTile(int col, int row)
	{
		return tiles[row][col];
	}
	/**
	 * A param�terk�nt megadott el�r�si �ton tal�lhat� f�jlb�l bet�lti a p�ly�t.
	 * @param filePath
	 */
	public void LoadMapFromFile(String filePath)
	{
		FileReader fr = null;
		try {
			fr = new FileReader(filePath);
			BufferedReader br = new BufferedReader(fr);
			//For the .jar
			//InputStream in = getClass().getResourceAsStream(filePath);
			//BufferedReader br = new BufferedReader(new InputStreamReader(in));
			int i = 0;
			while (true)
			{
				String line = br.readLine();
				if (line == null) break;
				String[] args = line.split(" ");
				for(int j = 0; j < args.length; j++)
				{
					BufferedImage subImage = tileSheet.getImage().getSubimage((Integer.parseInt(args[j]) % tileSheet.getWidth())*tileSheet.getTileWidth(), 
							(Integer.parseInt(args[j]) / tileSheet.getWidth())*tileSheet.getTileHeight(), tileSheet.getTileWidth(), tileSheet.getTileHeight());

					//TODO read type form file
					if (Integer.parseInt(args[j]) == 10 || 
						Integer.parseInt(args[j]) == 16 || 
						Integer.parseInt(args[j]) == 25 || 
						Integer.parseInt(args[j]) == 21 || 
						Integer.parseInt(args[j]) == 26 )
							tiles[i][j] = new WaterTile(subImage);
					else
						tiles[i][j] = new NormalTile(subImage);
				}
				i++;
				
			}
			br.close();
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		
	}
}
