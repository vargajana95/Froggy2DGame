package game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

public class Screen {
	private static final Dimension tileSize = new Dimension(48, 48);
	//private static final Dimension size = new Dimension(768, 665);
	private static final Dimension size = new Dimension(15*tileSize.width, 13*tileSize.height);
	
	/**
	 * @return az ablak m�retei.
	 */
	public static Dimension getSize()
	{
		return size;
	}
	/**
	 * @return egy Tile m�retei.
	 */
	public static Dimension getTileSize()
	{
		return tileSize;
	}
	/**
	 * Sz�veget rajzol a k�perny�re.
	 * @param screen a k�perny� grafik�ja
	 * @param text a sz�veg
	 * @param color a sz�veg sz�ne
	 * @param size a sz�veg m�rete
	 * @param x a sz�veg x koordin�tja
	 * @param y a sz�veg y koordin�tja
	 */
	public static void drawText(Graphics screen, String text, Color color, int size, int x, int y)
	{
		Graphics2D g2 = (Graphics2D)screen;
		
		Font font = new Font("Consolas", Font.PLAIN, size);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setColor(color);
        g2.setFont(font);
        FontMetrics metrics = g2.getFontMetrics(font);
    	g2.drawString(text, x, y+metrics.getAscent());
	}

}
