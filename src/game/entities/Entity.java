package game.entities;

import java.awt.Point;
import java.io.Serializable;

public abstract class Entity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected Point position;
	
	protected boolean removable = false;
	
	//TODO: remove this
	protected boolean collidable = true;
	
	public Entity(Point position)
	{
		this.position = position;
	}
	public boolean canCollide()
	{
		return collidable;
	}
	
	public void setPosition(int x, int y)
	{
		this.position.x = x;
		this.position.y = y;
	}
	public Point getPosition()
	{
		return position;
	}
	public boolean canRemove()
	{
		return removable;
	}
	
	public abstract void update();	
}
