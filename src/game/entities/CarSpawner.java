package game.entities;

import java.awt.Point;
import java.util.Random;

import game.GameManager;
import game.Screen;
import game.entities.movers.Car;
import game.entities.movers.Mover;

public class CarSpawner extends Spawner {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Random rand = new Random();
	private Mover.Direction direction;
	int carSpeed;
	
	/**
	 * nicializ�lja az oszt�lyt a konstruktorban megadott param�terekkel
	 * @param position a spawner poz�ci�ja
	 * @param direction az ir�ny, amibe a l�trehozott aut�k mozogni fognak (jobb vagy bal)
	 * @param delay a k�r�bel�li k�sleltet�s, amivel az egyes aut�k l�trhoz�sa k�vetni fogja egym�st.
	 * @param carSpeed a l�trehozott aut�k sebess�ge.
	 * @param gameManager a j�t�kot vez�rl� gameManager referenci�ja.
	 */
	public CarSpawner(Point position, Mover.Direction direction, int delay, int carSpeed, GameManager gameManager) {
		super(position, delay, gameManager);
		
		this.direction = direction;
		this.carSpeed = carSpeed;
				
		populateEntities();
	}
	/**
	 * L�trehozza az aut�t, amit majd k�s�bb be fog adni a j�t�kba.
	 */
	@Override
	protected Entity createEntityToSpawn() {
		
		int carType = rand.nextInt(100)%4;	
		
		return new Car("/CarSheet.png", new Point(position.x, position.y), direction, carSpeed, carType);
	}
	/**
	 * A p�ly�t teljesen felt�lti aut�kkal.
	 * Erre �ltal�ban az �j j�t�k kezd�s�n�l van sz�ks�g, hogy e legyen �res a p�lya.
	 */
	private void populateEntities()
	{

		Point pos = new Point(position.x, position.y);
		if (direction.equals(Mover.Direction.RIGHT))
		{
			while (pos.x <=Screen.getSize().width)
			{
				int carType = rand.nextInt(100)%4;
				gameManager.spawnEntity(new Car("/CarSheet.png", new Point(pos.x, pos.y), direction, carSpeed, carType));
				pos.x = pos.x + carSpeed*delay/1000*30;
			}
		}
		else if (direction.equals(Mover.Direction.LEFT))
		{
			while (pos.x > 0)
			{
				int carType = rand.nextInt(100)%4;
				gameManager.spawnEntity(new Car("/CarSheet.png", new Point(pos.x, pos.y), direction, carSpeed, carType));
				pos.x = pos.x - carSpeed*delay/1000*30;
			}
		}
	}

}
