package game.entities;

import java.awt.Graphics;

public interface DoubleLayered {
	public void drawSecondLayer(Graphics screen);
}
