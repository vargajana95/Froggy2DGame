package game.entities;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import game.Screen;
import game.graphics.TileSheet;
import game.player.Player;
import game.tiles.Tile;

public class HomeBox extends Entity implements Collidable, Drawable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Rectangle collisionRect;
	private TileSheet image;
	private Player owner;
	private boolean occupied = false;
	
	/**
	 * A megadott param�terekkel inicializ�lja a konstruktor az oszt�lyt.
	 * @param position a HomeBox poz�ci�ja
	 * @param collisionRect a HomeBox k�r�li t�glalap, amin bel�l �rz�keli a player-t
	 */
	public HomeBox(Point position, Rectangle collisionRect) {
		super(position);
		
		this.image = new TileSheet("/LilipadSheet.png");
		this.collisionRect = collisionRect;
	}

	/**
	 * Az egy frame alatt bk�vetez� esem�nyeket kezel� f�ggv�ny.
	 * Ebben az oszt�lyban nem csin�l semmit, csak az absztakt �soszt�ly miatt kell.
	 */
	public void update()
	{
		// TODO Make it possible to not call this method
	}
	/**
	 * Az egy frame-et kirajzol� f�ggv�ny
	 * @param screen a k�perny� grafik�ja
	 */
	@Override
	public void draw(Graphics screen) {
		screen.drawImage(image.getImage(), position.x, position.y, position.x+Screen.getTileSize().width, 
				position.y+Screen.getTileSize().height,
				0, 0, Screen.getTileSize().width, Screen.getTileSize().height, null);
		
		if (occupied && owner != null)
		{
			screen.drawImage(owner.getImage(), position.x, position.y, position.x+Screen.getTileSize().width,
					position.y+Screen.getTileSize().height,
					0, 480, Screen.getTileSize().width, 480+Screen.getTileSize().height, null);
		}
			
	}

	@Override
	public boolean isCollidingWith(Entity actor) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isCollidingWith(Tile tile) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isColliding() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Megvizsg�lj,a hogy a param�terk�nt kapott enitity-vel �tk�zik-e
	 */
	@Override
	public void onCollision(Entity callerEnt) {
		owner = (Player) callerEnt;
		occupied = true;
		owner.increaseOccupiedHomeBoxes();
		owner.reSpawn();
		//GameManager.getGameMode().onHomeEnter();
	}

	/**
	 * A HomeBox collisionRect-�vel t�r vissza.
	 */
	@Override
	public Rectangle getCollisionRect() {
		return collisionRect;
	}

}
