package game.entities;

import java.awt.Rectangle;

import game.tiles.Tile;

public interface Collidable {
	public boolean isCollidingWith(Entity actor);
	public boolean isCollidingWith(Tile tile);
	public boolean isColliding();
	public void onCollision(Entity callerEnt);
	public Rectangle getCollisionRect();
}
