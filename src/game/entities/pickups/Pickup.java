package game.entities.pickups;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import game.GameManager;
import game.Screen;
import game.entities.Collidable;
import game.entities.DoubleLayered;
import game.entities.Drawable;
import game.entities.Entity;
import game.graphics.TileSheet;

public abstract class Pickup extends Entity implements Drawable, DoubleLayered, Collidable {

	private static final long serialVersionUID = 1L;
	protected TileSheet image;
	protected Rectangle collisionRect;
	//removes itself in 10 seconds
	private final long timeToRemove = 10000;
	protected long spawnTime;
	
	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt
	 * @param position a pickup poz�ci�ja
	 */
	public Pickup(Point position) {
		super(position);
		
		this.image = new TileSheet("/gift.png");
		collisionRect = new Rectangle(position.x, position.y, Screen.getTileSize().width, Screen.getTileSize().height);
		spawnTime = GameManager.getTime();
	}
	/**
	 * A pickup-ot kirajzol� met�dus.
	 */
	@Override
	public void drawSecondLayer(Graphics screen) 
	{
		screen.drawImage(image.getImage(), position.x, position.y, null);
	}
	/**
	 * A pickup-ot friss�t� f�ggv�ny.
	 */
	@Override
	public void update() {
		if (GameManager.getTime() - spawnTime >= timeToRemove)
			removable = true;

	}

	@Override
	public void draw(Graphics screen) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * @return a pickup collisionRect-e
	 */
	@Override
	public Rectangle getCollisionRect() {
		// TODO Auto-generated method stub
		return collisionRect;
	}
}
