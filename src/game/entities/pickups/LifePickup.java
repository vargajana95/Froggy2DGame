package game.entities.pickups;

import java.awt.Graphics;
import java.awt.Point;

import game.entities.Entity;
import game.player.Player;
import game.tiles.Tile;

public class LifePickup extends Pickup {

	private static final long serialVersionUID = 1L;

	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt
	 * @param position a pickup poz�ci�ja
	 */
	public LifePickup(Point position) {
		super(position);
	}

	@Override
	public boolean isCollidingWith(Entity actor) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCollidingWith(Tile tile) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isColliding() {
		// TODO Auto-generated method stub
		return false;
	}
	/**
	 * Ha egy entity felveszi a pickup-ot, megh�v�dik ez a f�ggv�ny
	 * @param callerEnt az entity, akvel �tk�z�tt.
	 */
	@Override
	public void onCollision(Entity callerEnt) {
		Player player = (Player) callerEnt;
		
		player.addLife();
		this.removable = true;
	}

	
	
	

}
