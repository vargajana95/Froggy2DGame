package game.entities.pickups;

import java.awt.Point;
import java.util.Random;

import game.GameManager;
import game.Screen;
import game.entities.Entity;
import game.entities.Spawner;

public class PickupSpawner extends Spawner {

	private static final long serialVersionUID = 1L;
	private int timeBetweenSpawns;
	
	/**
	 * a kapott param�terekkel inicializ�lja az oszt�lyt.
	 * @param position a spawner poz�ci�ja
	 * @param delay a k�r�bel�li k�sleltet�s, amivel az egyes pickupok l�trhoz�sa k�vetni fogja egym�st.
	 * @param gameManager a j�t�kot vez�rl� gameManager referenci�ja.
	 */
	public PickupSpawner(Point position, int delay, GameManager gameManager) {
		super(position, delay, gameManager);
		
		timeBetweenSpawns = delay;
	}
	/**
	 * Az egy frame alatt bek�vetkez� esem�nyek.
	 * Ha el�g id� eltelt az el�z� pickup l�trehoz�sa �ta, �jjat hoz l�tre.
	 */
	public void update()
	{
		Random rand = new Random();
		//interval [timeBetweenSpawns, 2*timeBetweenSpawns]
		delay = rand.nextInt(timeBetweenSpawns) + timeBetweenSpawns;
		
		super.update();
	}
	/**
	 * L�trehozza a k�vetkez� pickup-ot amit be feg ker�lni a j�t�kba.
	 * @return a l�trehozott pickup.
	 */
	@Override
	protected Entity createEntityToSpawn() {
		Random rand = new Random();
		Pickup pickup = null;
		Point pos = new Point();
		int n = rand.nextInt(2) + 0;
		pos.x = rand.nextInt(Screen.getSize().width/Screen.getTileSize().width)*Screen.getTileSize().width;
		//We don't want to spawn in the first row
		pos.y = (rand.nextInt(Screen.getSize().height/Screen.getTileSize().height-1)+1)*Screen.getTileSize().height;
		
		//TODO: this spawner should have a list or set with the available pickups
		//That way it would be easier to add a new pickup
		switch (n)
		{
		case 0: pickup = new LifePickup(pos); break;
		case 1: pickup = new InvinciblePickup(pos); break;
		}
		
		return pickup;
	}

}
