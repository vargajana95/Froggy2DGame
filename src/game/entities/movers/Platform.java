package game.entities.movers;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import game.Screen;
import game.entities.Entity;
import game.player.Player;
import game.tiles.Tile;

public class Platform extends Mover {

	private static final long serialVersionUID = 1L;

	/**
	 * A kapott param�terekkel inicializ�laj az oszt�lyt.
	 * @param imagePath A platform k�p�nek relat�v el�r�si �tvonala
	 * @param position a latform  poz�ci�ja
	 * @param collisionRect a platform-ot k�r�fog� t�glalap.
	 * @param direction a platform mozg�is ir�nya.
	 * @param speed a platform mozg�si sebess�ge.
	 */
	public Platform(String imagePath, Point position, Rectangle collisionRect, Direction direction, int speed) {
		super(imagePath, position, collisionRect, direction);
		
		this.direction = direction;
		this.speed = speed;
	}

	/**
	 * Kirajzolja a platformot.
	 */
	@Override
	public void draw(Graphics screen) {
		screen.drawImage(tileSheet.getImage(), position.x, position.y, position.x+2*Screen.getTileSize().width, position.y+2*Screen.getTileSize().height, 
				0, 0, 2*Screen.getTileSize().width, 2*Screen.getTileSize().height, null);
		/*screen.setColor(Color.RED);
		screen.drawRect(collisionRect.x, collisionRect.y, collisionRect.width, collisionRect.height);*/
	}
	/**
	 * Mozgatja a platformot frameenk�nt a mozg�si ir�nyba..
	 */
	@Override
	public void update() {
		if (direction.equals(Direction.RIGHT))
		{
			position.x += speed;
			if (position.x > Screen.getSize().width)
				removable = true;
		}
		else if (direction.equals(Direction.LEFT))
		{
			position.x -= speed;
			if (position.x < -3*Screen.getTileSize().width)
				removable = true;
		}
		refreshCollisionRect();
	}
	@Override
	public boolean isCollidingWith(Entity actor) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCollidingWith(Tile tile) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isColliding() {
		// TODO Auto-generated method stub
		return false;
	}
	/**
	 * Ha egy entity-vel �tkk�zik a platform, megh�v�dik ez a f�ggv�ny
	 * @param callerEnt az entity, akvel �tk�z�tt.
	 */
	@Override
	public void onCollision(Entity callerEnt) {
		((Player) callerEnt).setOnPlatform(this);
	}

}
