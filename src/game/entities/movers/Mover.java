package game.entities.movers;

import java.awt.Point;
import java.awt.Rectangle;

import game.entities.Collidable;
import game.entities.Drawable;
import game.entities.Entity;
import game.graphics.TileSheet;

public abstract class Mover extends Entity implements Drawable, Collidable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected TileSheet tileSheet;
	
	protected int speed = 10;
	
	protected Direction direction;
	
	protected Rectangle collisionRect; 
	
	public enum Direction 
	{
		LEFT, RIGHT
	}
	
	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt.
	 * @param imagePath A mover k�p�nek relat�v el�r�si �tvonala
	 * @param position a mover poz�ci�ja
	 * @param collisionRect a mover-t k�r�fog� t�glalap.
	 * @param direction a mover mozg�is ir�nya.
	 */
	public Mover(String imagePath, Point position, Rectangle collisionRect, Direction direction) {
		super(position);
		
		this.direction = direction;
		this.collisionRect = collisionRect;
		
		tileSheet = new TileSheet(imagePath);
	}
	
	/**
	 * Az egyf frame alatt bek�vetkez� esem�neket kezel� f�ggv�ny.
	 * Az egyes lesz�rmazottaknak kell megval�sitaniuk.
	 */
	public abstract void update();
	
	/**
	 * A collisionRect-et igaz�tja a poz�ci�hoz.
	 */
	protected void refreshCollisionRect()
	{
		collisionRect.x = position.x;
		collisionRect.y = position.y;
	}
	/**
	 * @return a mover collisionRect-je
	 */
	public Rectangle getCollisionRect()
	{
		return collisionRect;
	}
	/**
	 * @return a moveer direction-ja
	 */
	//return -1 if direction is LEFT, 1 if RIGHT
	public int getDirection()
	{
		return direction.equals(Direction.LEFT) ? -1 : 1;
	}
	/**
	 * @return a mover sebess�ge.
	 */
	public int getSpeed()
	{
		return speed;
	}
	

}
