package game.entities.movers;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import game.Screen;
import game.entities.DoubleLayered;
import game.entities.Entity;
import game.player.Player;
import game.tiles.Tile;

public class Car extends Mover implements DoubleLayered {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int type;	
	
	/**
	 * A kapott param�terekkel inicializ�la az oszt�lyt.
	 * @param imagePath Az aut� k�p�nek relat�v el�r�si �tvonala
	 * @param position az aut� poz�ci�ja
	 * @param direction az aut� mozg�si ir�nya
	 * @param speed az aut� mozg�si sebess�ge
	 * @param type az aut� t�pusa
	 */
	public Car(String imagePath, Point position, Direction direction, int speed, int type) {
		super(imagePath, position, null, direction);
		
		this.type = type;
		this.speed = speed;
				
		if (type == 0 || type== 1)
		{
			collisionRect = new Rectangle(position.x, position.y, Screen.getTileSize().width, Screen.getTileSize().height);
			
		}
		else if (type == 2)
		{
			collisionRect = new Rectangle(position.x, position.y, 2*Screen.getTileSize().width, Screen.getTileSize().height);
		}
		else if (type == 3)
		{
			collisionRect = new Rectangle(position.x, position.y, 3*Screen.getTileSize().width, Screen.getTileSize().height);
		}
		
	}
	
	/**
	 * A player alatti dolgokat kirajzol� f�ggv�ny
	 * Mivel az aut�k a p�ly�n a player f�l�tt jelennek meg, ez�rt �res.
	 */
	public void draw(Graphics screen)
	{
	}
	
	/**
	 * A player f�l�tti dolgokat kirajzol� f�ggv�ny.
	 */
	@Override
	public void drawSecondLayer(Graphics screen) {
		int dir = 0;
		if (direction.equals(Mover.Direction.RIGHT))
			dir = 0;
		else if (direction.equals(Mover.Direction.LEFT))
			dir = 1;
		
		screen.drawImage(tileSheet.getImage(), position.x, position.y, position.x+collisionRect.width, position.y+collisionRect.height, 
				dir*collisionRect.width, type*collisionRect.height, dir*collisionRect.width+collisionRect.width, type*collisionRect.height+collisionRect.height, null);		
	}
	
	/**
	 * Az egy frame alatt bek�vetkez� esem�nyeket kezel� f�ggv�ny.
	 * Az aut�t mozgatja
	 */
	@Override
	public void update() {
		
		if (direction.equals(Direction.RIGHT))
		{
			position.x += speed;
			if (position.x > Screen.getSize().width)
				removable = true;
		}
		else if (direction.equals(Direction.LEFT))
		{
			position.x -= speed;
			if (position.x < -this.collisionRect.width)
				removable = true;
		}
		refreshCollisionRect();
	}

	@Override
	public boolean isCollidingWith(Entity actor) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCollidingWith(Tile tile) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isColliding() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Ha egy entity-vel �tkk�zik az aut�, megh�v�dik ez a f�ggv�ny
	 * @param callerEnt az entity, akvel �tk�z�tt.
	 */
	@Override
	public void onCollision(Entity callerEnt) {
		((Player) callerEnt).kill();
	}




}
