package game.entities;

import java.awt.Point;
import java.awt.Rectangle;

import game.GameManager;
import game.Screen;
import game.entities.movers.Mover;
import game.entities.movers.Mover.Direction;
import game.entities.movers.Platform;

public class PlatformSpawner extends Spawner {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int speed;
	private Direction direction;

	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt.
	 * @param position a spawner poz�ci�ja
	 * @param delay a k�r�bel�li k�sleltet�s, amivel az egyes platformok l�trhoz�sa k�vetni fogja egym�st.
	 * @param direction az ir�ny, amibe a l�trehozott platformok mozogni fognak (jobb vagy bal)
	 * @param gameManager a j�t�kot vez�rl� gameManager referenci�ja.
	 * @param speed a l�trehozott platformok sebess�ge.
	 */
	public PlatformSpawner(Point position, int delay, Direction direction, GameManager gameManager, int speed) {
		super(position, delay, gameManager);
	 
		this.direction = direction;
		this.speed = speed;
		populateEntities();
	}

	/**
	 * L�trehozza a platformot, amit majd k�s�bb be fog adni a j�t�kba.
	 */
	@Override
	protected Entity createEntityToSpawn() {
		
		if (direction.equals(Mover.Direction.RIGHT))
		{
			return new Platform("/TrunkSheet.png", new Point(position.x, position.y), 
					new Rectangle(position.x, position.y, 2*Screen.getTileSize().width, Screen.getTileSize().height), direction, speed);
		}
		else
		{
			return new Platform("/LilipadSheet.png", new Point(position.x, position.y), 
					new Rectangle(position.x, position.y, 2*Screen.getTileSize().width, Screen.getTileSize().height), direction, speed);
		}
		
	}
	/**
	 * A p�ly�t teljesen felt�lti platformokkal.
	 * Erre �ltal�ban az �j j�t�k kezd�s�n�l van sz�ks�g, hogy e legyen �res a p�lya.
	 */
	private void populateEntities()
	{

		Point pos = new Point(position.x, position.y);
		if (direction.equals(Mover.Direction.RIGHT))
		{
			while (pos.x <=Screen.getSize().width)
			{
				gameManager.spawnEntity(new Platform("/TrunkSheet.png", new Point(pos.x, pos.y), 
						new Rectangle(pos.x, pos.y, 2*Screen.getTileSize().width, Screen.getTileSize().height), direction, speed));
				pos.x = pos.x + speed*delay/1000*30;
			}
		}
		else if (direction.equals(Mover.Direction.LEFT))
		{
			while (pos.x > 0)
			{
				gameManager.spawnEntity(new Platform("/LilipadSheet.png", new Point(pos.x, pos.y), 
						new Rectangle(pos.x, pos.y, 2*Screen.getTileSize().width, Screen.getTileSize().height), direction, speed));
				pos.x = pos.x - speed*delay/1000*30;
			}
		}
	}

}
