package game.entities;

import java.awt.Point;

import game.GameManager;

public abstract class Spawner extends Entity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected long lastSpawned = 0;
	//The delay between spawns in milliseconds
	protected int delay;
	protected GameManager gameManager;
			
	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt
	 * @param position a spawner poz�ci�ja
	 * @param delay a k�r�bel�li k�sleltet�s, amivel az egyes objektumok l�trhoz�sa k�vetni fogja egym�st.
	 * @param gameManager a j�t�kot vez�rl� gameManager referenci�ja.
	 */
	public Spawner(Point position, int delay, GameManager gameManager) {
		super(position);
		if (delay < 100)
			delay = 100;
		
		this.delay = delay;
		this.gameManager = gameManager;
		
		collidable = false;
	}
	
	/**
	 * Az egy frame alatt bek�vetkez� esem�nyek.
	 * Ha el�g id� eltelt az el�z� entity l�trehoz�sa �ta, �jjat hoz l�tre.
	 */
	@Override
	public void update() {
		//long now = System.currentTimeMillis();
		long now = GameManager.getTime();
				
		if (delay <= (now -lastSpawned))
		{
			spawn();
		}
	}
	/**
	 * Beadja a j�t�kba a l�trehozott entity-t
	 */
	private void spawn()
	{
		lastSpawned = GameManager.getTime();
		gameManager.spawnEntity(createEntityToSpawn());
	}
	/**
	 * L�trehozza a k�vetkez� entity-t amit be feg ker�lni a j�t�kba.
	 * Az egyes lesz�rmazottaknak kell megval�s�taniuk a met�dust.
	 * @return a l�trehozott enitity.
	 */
	protected abstract Entity createEntityToSpawn();
	
}
