package game.entities;

import java.awt.Graphics;

public interface Drawable {
	public void draw(Graphics screen);
}
