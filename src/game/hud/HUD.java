package game.hud;

import java.awt.Color;
import java.awt.Graphics;

import game.Screen;
import game.entities.Drawable;
import game.player.Player;

public class HUD implements Drawable{

	private Player player;
	
	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt.
	 * @param player az aktu�lis player
	 */
	public HUD(Player player)
	{
		this.player = player;
	}
	
	/**
	 * A kirajzol� f�ggv�ny. Ki�rja a playerr�l az aktu�lis adatokat.
	 */
	@Override
	public void draw(Graphics screen) { 
		Screen.drawText(screen, ("Lifes:" + player.getLifes()), Color.WHITE, 20, 10, 10);
		Screen.drawText(screen, ("Score:" + player.getScore()), Color.WHITE, 20, 10, 30);
	}
}
