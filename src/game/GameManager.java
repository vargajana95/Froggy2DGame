package game;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JPanel;

import game.entities.CarSpawner;
import game.entities.DoubleLayered;
import game.entities.Drawable;
import game.entities.Entity;
import game.entities.HomeBox;
import game.entities.PlatformSpawner;
import game.entities.movers.Mover;
import game.entities.pickups.PickupSpawner;
import game.hud.HUD;
import game.menu.GameOverMenu;
import game.menu.MainMenu;
import game.menu.Menu;
import game.menu.PausedMenu;
import game.menu.WinMenu;
import game.player.Player;
import game.world.Map;

public class GameManager extends JPanel implements KeyListener, Serializable {
	private static final long serialVersionUID = 1L;
		
	protected transient Map map;
	protected List<Entity> entities;
	
	//A Map containing the added or removed entities
	// the value is 0 then the entity was added, if 1 then it needs to be removed
	protected java.util.Map<Entity, Integer> modifiedEntities;
	
	protected Player player;
	private transient HUD hud;
	private int homeBoxesToWin = 5;
		
	//private GameMode gameMode = new Classic(this);
	private transient Menu menu = new MainMenu(this);
	private boolean paused = true;
	
	//The time elapsed playing the game in milliseconds
	private static long time = 0;
	private long lastTime = 0;
	private boolean started = false;
	private int level = 1;

	
	public GameManager() 
	{		
		init();
	}
	/**
	 * Be�ll�tja az ablak param�tereit.
	 * L�trehozza a player-t
	 */
	private void init()
	{
		//this.input = input;
		setPreferredSize(Screen.getSize());
		addKeyListener(this);
		setFocusable(true);
		requestFocus();	
		lastTime = System.currentTimeMillis();
		
		//player = new Player("/player.png", new Point(448, 768));
		player = new Player("/player.png", new Point(360, 576));
	}
	
	/**
	 * Az ablak tartalm�t kirajzol� f�ggv�ny
	 * @param screen a grafika, amire kirajzolja
	 */
	public void paintComponent(Graphics screen)
	{
		//super.paintComponent(screen);
		
		//((Graphics2D) screen).scale(0.8, 0.8);
		if (started)
			draw(screen);
		if (paused && menu != null)
			menu.draw(screen);
		
				
	}
	/**
	 * Ez a met�dus fut v�gtelen�tett ciklusban.
	 */
	public void run()
	{
		long deltaTime = System.currentTimeMillis() - lastTime;
		lastTime = System.currentTimeMillis();
		
		
		if (paused && menu != null)
			menu.update();
		else
		{
			time += deltaTime;
			update();
		}
		Keys.update();
	}
	
	/**
	 * A f� rajzol� f�ggv�ny. Megh�vja az egyes kisebb komponensek rajzol� f�ggv�ny�t.
	 * @param screen erre a grafik�ra rajzol
	 */
	public void draw(Graphics screen)
	{
		map.draw(screen);
		
		for (Entity entity : entities)
		{
			if (entity instanceof Drawable)
				((Drawable) entity).draw(screen);
		}
				
		player.draw(screen);
		hud.draw(screen);
		
		for (Entity entity : entities)
		{
			if (entity instanceof DoubleLayered)
				((DoubleLayered) entity).drawSecondLayer(screen);
		}
	
	}

	/**
	 * Az egy frame alatt lej�tsz�d� esem�nyeket kezel� f�ggv�ny.
	 * A billenty�zetet olvassa.
	 * Megh�vja az egyes kisebb komponensek update() met�dsait.
	 */
	public void update() {
		if (Keys.isKeyPressed(KeyEvent.VK_ESCAPE))
		{
			setPause(true);
			setMenu(new PausedMenu(this));
		}
		
		if(Keys.isKeyPressed(KeyEvent.VK_W))
			player.goUp();
		else if(Keys.isKeyPressed(KeyEvent.VK_S))
			player.goDown();
		else if(Keys.isKeyPressed(KeyEvent.VK_D))
			player.goRight();
		else if(Keys.isKeyPressed(KeyEvent.VK_A))
			player.goLeft();

		
		player.update();
		
		if (!player.isDead() && !player.isRespawning())
		{
			player.checkForEntityCollision(entities);
			player.checkForTileCollision(map);
		}
		
		if (player.isDead())
		{
			setPause(true);
			setMenu(new GameOverMenu(this));
		}
		
		//for (Entity entity : entities)
		//for (ListIterator<Entity> entity = entities.listIterator(); entity.hasNext(); )
		for (int i = 0; i < entities.size(); i++)
		{
			Entity entity = entities.get(i);
			entity.update();
			if (entity.canRemove()) 
			{
				entities.remove(i);
				i--;
			}
		}
		
		checkForWin(player);
		
		//All updates are done
		//Entities added to the word now can be added
		refreshEntities();
		
	}

	/**
	 * �j j�t�kot ind�t az adott szinten. Felt�lti a p�ly�t, �s 
	 * be�ll�tja a szint kezd�s�hez sz�ks�ges attrib�tumokat.  
	 */
	public void newGame()
	{
		//I set it to zero, because I don't want the spawners to spawn immediately
		time = 0;
		started = true;
		
		entities = new ArrayList<Entity>();
		modifiedEntities = new HashMap<Entity, Integer>();
		
		map = new Map();
		map.LoadMapFromFile("res/maps/map01.map");
		
		hud = new HUD(player);
		//this.input = input;
		player.newGame();
		
		entities.add(new HomeBox(new Point(48, 0), new Rectangle(48, 0, Screen.getTileSize().width, Screen.getTileSize().height)));
		entities.add(new HomeBox(new Point(4*48, 0), new Rectangle(4*48, 0, Screen.getTileSize().width, Screen.getTileSize().height)));
		entities.add(new HomeBox(new Point(7*48, 0), new Rectangle(7*48, 0, Screen.getTileSize().width, Screen.getTileSize().height)));
		entities.add(new HomeBox(new Point(10*48, 0), new Rectangle(10*48, 0, Screen.getTileSize().width, Screen.getTileSize().height)));
		entities.add(new HomeBox(new Point(13*48, 0), new Rectangle(13*48, 0, Screen.getTileSize().width, Screen.getTileSize().height)));
		//entities.add(new Car("/CarSheet.png", new Point(65, 512), new Rectangle(65, 512, 64, 64), Mover.Direction.RIGHT));
		//entities.add(new Platform("/TrunkSheet.png", new Point(-576, 64), new Rectangle(-576, 64, 128, 64), Mover.Direction.RIGHT));
		
		entities.add(new CarSpawner(new Point(-3*48, 7*48), Mover.Direction.RIGHT, 2500-level*100, 6+level, this));
		entities.add(new CarSpawner(new Point(Screen.getSize().width+2*48, 8*48), Mover.Direction.LEFT, 2800-level*100, 4+level, this));
		entities.add(new CarSpawner(new Point(-3*48, 9*48), Mover.Direction.RIGHT, 3000-level*80, 3+level/2, this));
		entities.add(new CarSpawner(new Point(Screen.getSize().width+2*48, 10*48), Mover.Direction.LEFT, 4000-level*50, 3+level/2, this));
		entities.add(new CarSpawner(new Point(-3*48, 11*48), Mover.Direction.RIGHT, 4500-level*50, 3+level/2, this));
		//entities.add(new CarSpawner(new Point(640+192, 576), Mover.Direction.LEFT, 2700, 9, this));
		
		entities.add(new PlatformSpawner(new Point(-2*48, 48), 2500+level*100, Mover.Direction.RIGHT, this, 5));
		entities.add(new PlatformSpawner(new Point(Screen.getSize().width+2*48, 2*48), 2100+level*100, Mover.Direction.LEFT, this, 5));
		entities.add(new PlatformSpawner(new Point(-2*48, 3*48), 2300+level*150, Mover.Direction.RIGHT, this, 5));
		entities.add(new PlatformSpawner(new Point(Screen.getSize().width+2*48, 4*48), 1900+level*150, Mover.Direction.LEFT, this, 5));
		entities.add(new PlatformSpawner(new Point(-2*48, 5*48), 2000+level*200, Mover.Direction.RIGHT, this, 5));
		
		/*entities.add(new Platform("/TrunkSheet.png", new Point(-128, 192), new Rectangle(-128, 192, 128, 64),Mover.Direction.RIGHT));
		entities.add(new Platform("/TrunkSheet.png", new Point(-128+400, 192), new Rectangle(-128+400, 192, 128, 64),Mover.Direction.RIGHT));
		
		
		entities.add(new Platform("/TrunkSheet.png", new Point(-128+50, 64), new Rectangle(-128+50, 64, 128, 64),Mover.Direction.RIGHT));
		entities.add(new Platform("/TrunkSheet.png", new Point(-128+450, 64), new Rectangle(-128+450, 64, 128, 64),Mover.Direction.RIGHT));
		
		entities.add(new Platform("/LilipadSheet.png", new Point(640, 128), new Rectangle(640, 128, 128, 64),Mover.Direction.LEFT));
		entities.add(new Platform("/LilipadSheet.png", new Point(640-400, 128), new Rectangle(640-400, 128, 128, 64),Mover.Direction.LEFT));
		*/
		//entities.add(new PlatformSpawner(new Point(640+128, 192), 1300, 1, this));
		
		entities.add(new PickupSpawner(new Point(0, 0),  10000,  this));
		
	}
	/**
	 * Megn�zi, hogy a param�terk�nt kapott player el�rte-e m�r a szint megnyer�s�hez sz�ks�ges
	 * felt�teleket.
	 * @param player a Player, akit vizsg�l
	 */
	public void checkForWin(Player player) {
		if (player.getNumberOfoccupiedHomeBoxes() == homeBoxesToWin)
		{
			setPause(true);
			setMenu(new WinMenu(this));
			level++;
		}
	}
	
	/**
	 * A param�terk�nt kapott entity-t feljegyzi, hogy be kell adni a j�t�kba.
	 * @param entity az Entity, akit a list�ba szeretn�nk tenni
	 */
	public void spawnEntity(Entity entity)
	{
		modifiedEntities.put(entity, 0);
	}
	/**
	 * A param�terk�nt kapott entity-t feljegyti, hogy ki kell venni a j�t�kb�l.
	 * @param entity az Entity, akit t�r�lni szeretn�nk a j�t�kb�l.
	 */
	public void removeEntity(Entity entity)
	{
		modifiedEntities.put(entity, 1);
	}
	/**
	 * Ha entity-t addtak be a j�t�kba, vagy t�r�ltek a j�t�kb�l, akkor
	 * ez a f�ggv�ny t�ylegesen-be is addja. 
	 */
	public void refreshEntities()
	{
		for (java.util.Map.Entry<Entity, Integer> entry : modifiedEntities.entrySet())
		{
			if (entry.getValue() == 0)
				entities.add(entry.getKey());
			else if (entry.getValue() == 1)
				if (entities.contains(entry.getKey()))
					entities.remove(entry.getKey());
			
		}
		modifiedEntities.clear();
	}
	/**
	 * Visszaadja a player-t.
	 * @return a player
	 */
	public Player getPlayer()
	{
		return player;
	}
	/**
	 * Visszaadja a p�ly�t (map-et).
	 * @return a map
	 */
	public Map getWorld()
	{
		return map;
	}
	/**
	 * @return a j�t�kban l�v� entity-k list�ja.
	 */
	public List<Entity> getEntities()
	{
		return entities;
	}
	
	/*public GameMode getGameMode()
	{
		return gameMode;
	}*/
	/**
	 * A param�terk�nt kapott men�t �ll�tja be akt�vnak.
	 * @param menu a be�ll�tand� men�
	 */
	public void setMenu(Menu menu)
	{
		this.menu = menu;
	}
	/**
	 * A param�terk�nt kapott boolean-ra �ll�tja a paused attrib�tumot.
	 * @param paused az �j �rt�k
	 */
	public void setPause(boolean paused)
	{
		this.paused = paused;
	}
	/**
	 * @return a szint elind�t�s�t�l eltelt id� ms-ben.
	 */
	public static long getTime()
	{
		return time;
	}
	/**
	 * @return az aktu�lis szint sz�ma
	 */
	public int getLevel()
	{
		return level;
	}
	/**
	 * Elmenti az aktu�lis j�t�kot k�s�bbi bet�lt�sre.
	 */
	public void saveGame()
	{
		try {
			FileOutputStream f = new FileOutputStream("saveGame.save");
			ObjectOutputStream out = new ObjectOutputStream(f);
			out.writeObject(entities);
			out.writeObject(modifiedEntities);
			out.writeObject(player);
			out.writeObject(time);
			//out.writeObject(lastTime);
			out.writeObject(level);
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Visszat�lti a kor�bban elmentett j�t�kot.
	 */
	public void loadGame()
	{
		
		init();
		try {
			FileInputStream f = new FileInputStream("saveGame.save");
			ObjectInputStream in = new ObjectInputStream(f);
			entities = (List<Entity>) in.readObject();
			modifiedEntities = (java.util.Map<Entity, Integer>) in.readObject();
			map = new Map();
			map.LoadMapFromFile("res/maps/map01.map");
			player = (Player) in.readObject();
			time = (long) in.readObject();
			//lastTime = (long) in.readObject();
			level = (int) in.readObject();
			in.close();
		} catch (IOException | ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		
		started = true;
		hud = new HUD(player);
		setMenu(null);
		//setPause(false);
	}
	
	
	
	/**
	 * Ha a felhaszn�� lenyom egy gombot a billenty�zeten
	 * megh�v�dik ez a met�dus.
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		Keys.keyPress(e.getKeyCode());
	}
	
	/**
	 * Ha a felhaszn�l� elenged egy kor�bban lenyomott billenty�t, 
	 * megh�v�dik ez a met�dus.
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		Keys.keyRelease(e.getKeyCode());		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}
}
