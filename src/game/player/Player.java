package game.player;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.List;

import game.GameManager;
import game.Screen;
import game.entities.Collidable;
import game.entities.Drawable;
import game.entities.Entity;
import game.entities.movers.Platform;
import game.graphics.TileSheet;
import game.tiles.Tile;
import game.world.Map;

public class Player extends Entity implements Drawable, Collidable {
	
	private static final long serialVersionUID = 352570086809471469L;
	public enum Animations {
		MOVE_UP,  MOVE_LEFT, MOVE_DOWN, MOVE_RIGHT, NONE, NUMOFMOVES
	}
	
	private Point nextPosition;
	private int speed = 8;
	private AnimationTable currentAnim;
	private TileSheet animSheet;
	private Rectangle collisionRect;
	private int lifes = 3;
	private int occupiedHomeBoxes = 0;
	//Time between respawns
	private long resetTimer = 2000;
	private long killedTime;
	private boolean shouldRespawn = false;
	private boolean dead = false;
	private boolean jumping = true;
	//The platform the player is on, null if none
	private Platform platform = null;
	private int score;
	//The player got score when he stepped on this X kkordinate
	private int lastScoreY;
	
	//This is the position the player is headed to
	private Point deltaPosition;
	
	private boolean invincible = false;
	private long invincibleTime;
	
	/**
	 * A kapott param�terekkel inicializ�lja aaz oszt�lyt.
	 * @param imagePath a player k�p�nek el�r�si �tja
	 * @param position a player kezdeti poz�ci�ja.
	 */
	public Player(String imagePath, Point position)
	{
		super(position);
		
		this.collisionRect = new Rectangle(position.x+15, position.y+19, 19, 11);
		animSheet = new TileSheet(imagePath);
		//currentAnim = new Animation();
		//currentAnim.setAnim(Animation.Move.NONE);
		this.nextPosition = new Point(position);
		lastScoreY = position.y;
		this.deltaPosition = new Point(0, 0);
		
		currentAnim = new AnimationTable(animSheet, Animations.NUMOFMOVES.ordinal());
		
	}
	/**
	 * A player-t framenk�nt kirajzol� met�dus.
	 */
	public void draw(Graphics screen)
	{
		if (shouldRespawn)
		//TODO we should play die animation here
			return;
		
		
		//TODO don't use 64 literals
		screen.drawImage(currentAnim.getFrame(), position.x, position.y-19,
				Screen.getTileSize().width, Screen.getTileSize().height, null);
		/*screen.setColor(Color.RED);
		screen.drawRect(collisionRect.x, collisionRect.y, collisionRect.width, collisionRect.height);
		screen.setColor(Color.GREEN);
		screen.drawRect(position.x+deltaPosition.x, position.y+deltaPosition.y, 64, 64);*/
	}
	/**
	 * A player-t framenk�nt friss�t� �s mozgat� f�ggv�nye.
	 */
	public void update()
	{
		//If the player is dead, we want to respawn him, if the time excided
		if (!dead && shouldRespawn)
		{
			if (System.currentTimeMillis() - killedTime >= resetTimer)
				reSpawn();
		}
			
		//If we are on a platform, we set the position
		if (platform != null)
			if (!collisionRect.intersects(platform.getCollisionRect()))
				platform = null;
		
		if (platform != null)
		{
			nextPosition.x += platform.getDirection()*platform.getSpeed();
		}
		
		if (jumping)
		{
			if (deltaPosition.x > 0)
			{
				if (deltaPosition.x < speed)
				{
					nextPosition.x += deltaPosition.x;
					deltaPosition.x = 0;
				}
				else
				{
					nextPosition.x+=speed;
					deltaPosition.x -= speed;
				}
				currentAnim.setAnimation(Animations.MOVE_RIGHT);
			}
			else if (deltaPosition.x < 0)
			{
				if (-deltaPosition.x < speed)
				{
					nextPosition.x += deltaPosition.x;
					deltaPosition.x = 0;
				}
				else
				{
					nextPosition.x-=speed;
					deltaPosition.x += speed;
				}
				currentAnim.setAnimation(Animations.MOVE_LEFT);
			}
			else if (deltaPosition.y > 0)
			{
				if (deltaPosition.y < speed)
				{
					nextPosition.y += deltaPosition.y;
					deltaPosition.y = 0;
				}
				else
				{
					nextPosition.y+=speed;
					deltaPosition.y -= speed;
				}
				currentAnim.setAnimation(Animations.MOVE_DOWN);
			}
			else if (deltaPosition.y < 0)
			{
				if (-deltaPosition.y < speed)
				{
					nextPosition.y += deltaPosition.y;
					deltaPosition.y = 0;
				}
				else
				{
					nextPosition.y-=speed;
					deltaPosition.y += speed;
				}
				currentAnim.setAnimation(Animations.MOVE_UP);
			}
			else
			{
				//The player has reached its destination
				if (nextPosition.y < lastScoreY)
				{
					score += 10;
					lastScoreY = nextPosition.y;
				}
				jumping = false;
				currentAnim.setAnimation(Animations.NONE);
			}
		}
		/*else
		{
			//This is called, when the player's position was changed from outside
			destination.x = nextPosition.x;
			destination.y = nextPosition.y;
		}*/
		
		
		position = new Point (nextPosition.x, nextPosition.y);

		currentAnim.play();
		
		refreshCollisionRect();
		if (isOutOfScreen())
			kill();
		
		checkInvincibleExpired();
	}
	/**
	 * A param�terk�nt kapott entity list�t v�gigj�rva megvizsg�lj,a hogy �tk�zik-e valamelyik
	 * entitivel.
	 * @param entities az entity lista.
	 */
	public void checkForEntityCollision(List<Entity> entities)
	{
		//We set the player not to be on a platform
		//If he is then the Platform's onCollision method will set it back to true
		//onPlatform = false;
		for (Entity entity : entities)
		{
			if(entity instanceof Collidable)
				if (this.collisionRect.intersects(((Collidable) entity).getCollisionRect()))
					((Collidable) entity).onCollision(this);
		}
	}
	/**
	 * A param�terk�nt kapott p�ly�n megn�zi, hogy �tk�zik-e az egyes tile-okkal.
	 * @param map a p�lya.
	 */
	public void checkForTileCollision(Map map)
	{
		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 10; j++)
			{
				if (isCollidingWithTile(i, j))
				{
					map.getTile(i, j).onCollision(this);
				}
						
			}
		}
	}
	
	/**
	 * A kapott param�terre �ll�tja a player poz�ci�j�t.
	 */
	public void setPosition(int x, int y)
	{
		//super.setPosition(x, y);
		/*position.x = x;
		position.y = y;*/
		nextPosition.x = x;
		nextPosition.y = y;
		/*//We need to set the destination as well
		destination.x = x;
		destination.y = y;*/
	}
	/**
	 * Visszat�r a player aktu�lis poz�ci�j�val.
	 */
	public Point getPosition()
	{
		return position;
	}
	/**
	 * @return a player k�pe.
	 */
	public BufferedImage getImage()
	{
		return animSheet.getImage();
	}
	/**
	 * A playert balra mozgat� f�ggv�ny.
	 */
	public void goLeft()
	{
		if (jumping)
			return;
				
		jumping = true;
		//We are moving on the x axis
		deltaPosition.y = 0;
		
		if (position.x-Screen.getTileSize().width < 0)
		{
			deltaPosition.x = 0;
			return;
		}
		if (platform != null)
		{
			deltaPosition.x = -Screen.getTileSize().width;
			return;
		}

		if ((deltaPosition.x+position.x)%Screen.getTileSize().width > Screen.getTileSize().width/2)
			deltaPosition.x =  -(position.x %Screen.getTileSize().width);
		else
			deltaPosition.x = -(position.x%Screen.getTileSize().width+Screen.getTileSize().width);
	}
	/**
	 * A playert jobbra mozgat� f�ggv�ny.
	 */
	public void goRight()
	{
		if (jumping)
			return;
		
		jumping = true;
		//We are moving on the x axis
		deltaPosition.y = 0;

		if (position.x + Screen.getTileSize().width +Screen.getTileSize().width  > Screen.getSize().width)
		{
			deltaPosition.x = 0;
			return;
		}
		if (platform != null)
		{
			deltaPosition.x = Screen.getTileSize().width;
			return;
		}
		
		if ((deltaPosition.x+position.x)%Screen.getTileSize().width > Screen.getTileSize().width/2)
			deltaPosition.x =  Screen.getTileSize().width-position.x%Screen.getTileSize().width+Screen.getTileSize().width;
		else
			deltaPosition.x = Screen.getTileSize().width-position.x%Screen.getTileSize().width;
	}
	/**
	 * A playert felfel� mozgat� f�ggv�ny.
	 */
	public void goUp()
	{
		if (jumping)
			return;
		
		jumping = true;
		//It is sure the we have jumped off the platform
		platform = null;
		//We are moving on the y axis
		deltaPosition.x = 0;
		
		if (position.y - Screen.getTileSize().height < 0)
			deltaPosition.y = 0;
		else
			deltaPosition.y = -Screen.getTileSize().height;
		
	}
	/**
	 * A playert lefel� mozgat� f�ggv�ny.
	 */
	public void goDown()
	{
		if (jumping)
			return;
		
		jumping = true;
		platform = null;
		//We are moving on the y axis
		deltaPosition.x = 0;
		
		if (position.y + Screen.getTileSize().height +Screen.getTileSize().height > Screen.getSize().height)
			deltaPosition.y = 0;
		else
			deltaPosition.y = Screen.getTileSize().height;
	}
	/**
	 * Az �j szint kezd�s�hez el�k�sz�ti a playert.
	 */
	public void newGame()
	{
		dead = false;
		lifes = 3;
		occupiedHomeBoxes = 0;
		reSpawn();
	}
	/**
	 * A playert a kezd�poz�ci�ba �ll�tja, �s �letet von le, ha m�g van el�g �lete.
	 */
	public void reSpawn()
	{
		shouldRespawn = false;
		nextPosition = new Point(360, 576);
		deltaPosition = new Point(0, 0);
		currentAnim.setAnimation(Animations.NONE);
	}
	/**
	 * Meg�li a playert.
	 */
	public void kill()
	{
		//TODO probably this should be somewhere else
		// not sure if we should not kill the player while he is jumping
		if (jumping || shouldRespawn || invincible)
			return;
		
		this.lifes--;
		if (lifes <= 0)
			this.dead = true;
		else
			shouldRespawn = true;
		killedTime = System.currentTimeMillis();
	}
	/**
	 * Megvizsg�lja, hogy a param�terk�nt kapott entity-vel �tk�zik-e.
	 */
	@Override
	public   boolean isCollidingWith(Entity entity) {
		if(entity instanceof Collidable)
			if (this.collisionRect.intersects(((Collidable) entity).getCollisionRect()))
				return true;
		return false;
	}
	/**
	 * Friss�ti a collisionRect-et az aktu�lis poz�ci�ra.
	 */
	protected void refreshCollisionRect()
	{
		collisionRect.x = position.x + 20;
		collisionRect.y = position.y +25;
	}
	/**
	 * @return a m�g megmaradt �letzek sz�ma.
	 */
	public int getLifes()
	{
		return lifes;
	}
	/**
	 * @return true, ha a player �pp platformon �ll, k�l�nben false.
	 */
	public boolean isOnPlatform()
	{
		return platform != null;
	}
	/**
	 * A kapott boolean �rt�kre �ll�tja a platform attrib�tumot.
	 */
	public void setOnPlatform(Platform platform)
	{
		if (!jumping)
			this.platform = platform;
	}
	/**
	 * @return true, ha a player kil�pett a k�perny�r�l, k�l�nben false.
	 */
	public boolean isOutOfScreen()
	{
		//TODO don't use 64
		Rectangle playerBox = new Rectangle(nextPosition.x, nextPosition.y, Screen.getTileSize().width, Screen.getTileSize().height);
		return !playerBox.intersects(new Rectangle(0, 0, Screen.getSize().width, Screen.getSize().height));
	}
	@Override
	public boolean isCollidingWith(Tile tile) {
		return false;
	}
	/**
	 * Megvizsg�lja, hogy a param�tereknek megfelel� tile-al �tk�zik-e
	 */
	public boolean isCollidingWithTile(int x, int y)
	{
		//TODO Don't use literals here
		Rectangle tileRect = new Rectangle(x*Screen.getTileSize().width, y*Screen.getTileSize().height, 
				Screen.getTileSize().width, Screen.getTileSize().height);
		return this.collisionRect.intersects(tileRect);
	}
	
	
	@Override
	public boolean isColliding() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void onCollision(Entity actor) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * Visszat�r a player collisionRect-�vel.
	 */
	@Override
	public Rectangle getCollisionRect() {
		// TODO Auto-generated method stub
		return collisionRect;
	}
	/**
	 * Megn�veli az occupiedHomeBoxes attrb�tum �rt�k�t.
	 */
	public void increaseOccupiedHomeBoxes()
	{
		occupiedHomeBoxes++;
		score+=155;
		lastScoreY = 560;
	}
	/**
	 * @return az occupiedHomeBoxes attrib�tum �rt�ke.
	 */
	public int getNumberOfoccupiedHomeBoxes()
	{
		return occupiedHomeBoxes;
	}
	/**
	 * @return true, ha a palyer meghalt, k�l�nben false.
	 */
	public boolean isDead()
	{
		return dead;
	}
	/**
	 * @return true, ha a player �ppen a kezd�poz�ci�ba �ll�t�sra v�r, k�lnben false.
	 */
	public boolean isRespawning()
	{
		return shouldRespawn;
	}
	/**
	 * @return true, ha a player �ppen mozog, k�l�nben false.
	 */
	public boolean isJumping()
	{
		return jumping;
	}
	/**
	 * @return a player aktu�lis pontsz�ma.
	 */
	public int getScore()
	{
		return score;
	}
	/**
	 * Megn�veli a player �leteinek sz�m�t.
	 */
	public void addLife()
	{
		lifes++;
	}
	/**
	 * Sebezhetetlenn� teszi a playert.
	 */
	public void setInvincible()
	{
		invincible = true;
		invincibleTime = GameManager.getTime();
	}
	/**
	 * @return true, ha a player �pp sebezhetetlen, k��nben false.
	 */
	public boolean isInvincible()
	{
		return invincible;
	}
	/**
	 * Megn�zi, hogy lej�rt-e m�r a sebezhetetens�g ideje, �s ha igen a player nem lesz sebezetetlen t�bb�.
	 */
	private void checkInvincibleExpired()
	{
		if (!invincible)
			return;
		//invincible for 3 seconds
		if (GameManager.getTime()-invincibleTime > 3000)
		{
			invincible = false;
		}
	}

}
