package game.player;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import game.Screen;
import game.graphics.TileSheet;
import game.player.Player.Animations;

public class AnimationTable implements Serializable {
	
	private static final long serialVersionUID = 1247970236802422257L;
	public class Animation {
		private transient BufferedImage[] frames;
		private int numberOfFrames;
		private int currentFrame;
		
		private Animation(BufferedImage frameLine)
		{
			numberOfFrames = frameLine.getWidth()/Screen.getTileSize().width;
			frames = new BufferedImage[numberOfFrames];
			
			for (int i = 0; i < frames.length; i++ )
			{
				frames[i] = frameLine.getSubimage(i*Screen.getTileSize().width, 0, Screen.getTileSize().width, Screen.getTileSize().height);
			}
			
			currentFrame = 0;
		}
		
		public void play()
		{
			currentFrame++;
			
			if (currentFrame == numberOfFrames)
			{
				currentFrame = 0;
			}
		}
		
		public BufferedImage getFrame()
		{
			return frames[currentFrame];
		}
	
		
	}
	
	private transient Animation[] animations;
	private Animations currentAnim; 
	private TileSheet animSheet;
	private int numOfAnims;
	/**
	 * A apott param�terekkel inicializ�lja az oszt�lyt.
	 * @param animSheet Az anim�ci� k�pe
	 * @param numOfAnims az anim�ci�k sz�ma
	 */
	//TODO get numOfAnims from the pg file
	public AnimationTable(TileSheet animSheet, int numOfAnims)
	{
		this.animSheet = animSheet;
		this.numOfAnims = numOfAnims;
		
		init();
	}
	/**
	 * Inicializ�lja a player anim�ci�it.
	 */
	private void init()
	{
		animations = new Animation[numOfAnims];
		
		//TODO Should not use the literals here
		// Use the Move enums instead, or read this from a file		
		animations[Animations.MOVE_UP.ordinal()] = new Animation(animSheet.getImage().getSubimage(0, 8*Screen.getTileSize().height, 9*Screen.getTileSize().width, Screen.getTileSize().height));
		animations[Animations.MOVE_DOWN.ordinal()] = new Animation(animSheet.getImage().getSubimage(0, 10*Screen.getTileSize().height, 9*Screen.getTileSize().width, Screen.getTileSize().height));
		animations[Animations.MOVE_LEFT.ordinal()] = new Animation(animSheet.getImage().getSubimage(0, 9*Screen.getTileSize().height, 9*Screen.getTileSize().width, Screen.getTileSize().height));
		animations[Animations.MOVE_RIGHT.ordinal()] = new Animation(animSheet.getImage().getSubimage(0, 11*Screen.getTileSize().height, 9*Screen.getTileSize().width, Screen.getTileSize().height));
		animations[Animations.NONE.ordinal()] = new Animation(animSheet.getImage().getSubimage(0, 10*Screen.getTileSize().height, Screen.getTileSize().width, Screen.getTileSize().height));
	
		currentAnim = Animations.NONE;
	}
	/**
	 * Lej�tsza az aktu�lis anim�ci�t.
	 */
	public void play()
	{
		animations[currentAnim.ordinal()].play();
	}
	/**
	 * @return az �pp aktu�lis anim�ci� aktu�lis k�pkock�ja.
	 */
	public BufferedImage getFrame()
	{
		return animations[currentAnim.ordinal()].getFrame();
	}
	/**
	 * A param�tek�nt kapott anim�ci�t �ll�tja be aktu�lisnak.
	 * @param anim az anim�ci�
	 */
	public void setAnimation(Animations anim)
	{
		currentAnim = anim;
	}
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        
        init();
	}
}
