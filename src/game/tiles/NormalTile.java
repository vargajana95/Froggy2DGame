package game.tiles;

import java.awt.image.BufferedImage;

import game.entities.Entity;

public class NormalTile extends Tile {
	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt.
	 * @param image a Tile k�pe.
	 */
	public NormalTile(BufferedImage image) {
		super(image);
	}
	/**
	 * Ha egy entity r�l�p a Tile megh�v�dik a met�dus.
	 * Ebben az esetben nem t�rt�nik semmi, a met�dus �res.
	 */
	@Override
	public void onCollision(Entity callerEnt) {
		// TODO Auto-generated method stub

	}

}
