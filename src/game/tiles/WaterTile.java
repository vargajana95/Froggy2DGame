package game.tiles;

import java.awt.image.BufferedImage;

import game.entities.Entity;
import game.player.Player;

public class WaterTile extends Tile {
	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt.
	 * @param image a Tile k�pe.
	 */
	public WaterTile(BufferedImage image) {
		super(image);
	}
	/**
	 * Ha egy entity r�l�p a Tile megh�v�dik a met�dus.
	 * Meg�li player-t, ha r�l�p.
	 */
	@Override
	public void onCollision(Entity callerEnt) {
		Player player = ((Player) callerEnt);
		
		if (player.isOnPlatform() == false && !player.isJumping())
		{
			//We don't want to kill the player, if it's on a platform
			player.kill();
		}

	}

}
