package game.tiles;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import game.entities.Entity;

public abstract class Tile {
	private transient BufferedImage image;
	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt.
	 * @param image a Tile k�pe.
	 */
	public Tile(BufferedImage image)
	{
		this.image = image;
	}
	/**
	 * A Tile-t kirajzol� f�ggv�ny.
	 * @param screen a k�perny�
	 * @param x az x poz�ci� a k�perny�n, ahova ki kell rajzolni
	 * @param y az y poz�ci� a k�perny�n, ahova ki kell rajzolni
	 */
	public void drawTile(Graphics screen, int x, int y)
	{
		screen.drawImage(image, x, y, null);
		/*screen.setColor(Color.RED);
		screen.drawRect(x, y, 64, 64);*/
	}
	
	public abstract void onCollision(Entity callerEnt);

}
