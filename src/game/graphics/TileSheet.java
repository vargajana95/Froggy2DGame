package game.graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import game.Screen;


public class TileSheet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private transient BufferedImage image = null;
	private String imageName;
	private int tileHeight = Screen.getTileSize().height;
	private int tileWidth = Screen.getTileSize().width;
	/**
	 * A kapott param�terrel kapott el�r�si ��tr�l bet�lti a k�pet.
	 * @param name a k� relat�v el�r�si �tja.
	 */
	public TileSheet(String name)
	{
		this.imageName = name;
		image = ImageSheets.getImage(name);
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        
        image = ImageSheets.getImage(imageName);
	}
	/**
	 * @return a k�p
	 */
	public BufferedImage getImage()
	{
		return image;
	}
	/**
	 * @return a k�p sz�less�ge
	 */
	public int getWidth()
	{
		return image.getWidth()/tileWidth;
	}
	/**
	 * @return a k�p magass�ga
	 */
	public int getHeight()
	{
		return image.getHeight()/tileHeight;
	}
	/**
	 * @return egy tile magass�ga
	 */
	public int getTileHeight()
	{
		return tileHeight;
	}
	/**
	 * @return egy tile sz�less�ge
	 */
	public int getTileWidth()
	{
		return tileWidth;
	}
	
	
}
