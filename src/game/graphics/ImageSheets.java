package game.graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

public class ImageSheets {
	private static Map<String, BufferedImage> images;
	
	static {
		images = new HashMap<String, BufferedImage>();
	}
	
	/**
	 * Visszat�r a param�terk�nt kapott el�r�si �ttal rendelkez� k�ppel.
	 * @param name a relat�v el�r�si �t.
	 * @return a k�p
	 */
	public static BufferedImage getImage(String name)
	{
		if (images.containsKey(name))
			return images.get(name);
		else
		{
			BufferedImage image = null;
			try {
				image = ImageIO.read(ImageSheets.class.getResourceAsStream(name));
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			images.put(name, image);
			return image;
		}
	}
}
