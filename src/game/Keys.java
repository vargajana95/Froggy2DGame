package game;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;


public class Keys {
	private static Map<Integer, Boolean> keyStates = new HashMap<>();
	private static Map<Integer, Boolean> prevKeyStates;
	
	static {
		keyStates.put(KeyEvent.VK_W, false);
		keyStates.put(KeyEvent.VK_A, false);
		keyStates.put(KeyEvent.VK_S, false);
		keyStates.put(KeyEvent.VK_D, false);
		keyStates.put(KeyEvent.VK_SPACE, false);
		keyStates.put(KeyEvent.VK_ESCAPE, false);
		keyStates.put(KeyEvent.VK_ENTER, false);
		
		prevKeyStates = new HashMap<Integer, Boolean>(keyStates);
	}
	
	/**
	 * Az egy frame alatt bek�vetkez� esem�nyeket kezel� f�ggv�ny az oszt�lyban.
	 */
	public static void update()
	{
		for (java.util.Map.Entry<Integer, Boolean> prevKey : prevKeyStates.entrySet())
		{
			prevKey.setValue(keyStates.get(prevKey.getKey()));
		}
	}
	/**
	 * A param�terk�nt kapott billenty�r�l feljegyzi, hogy meg lett nyomva.
	 * @param k a billenty� k�dja
	 */
	public static void keyPress(int k)
	{
		keyStates.replace(k, true);
	}
	/**
	 * A param�terk�nt kapott billenty�r�l feljegyzi, hogy el lett engedve.
	 * @param k a billenty� k�dja
	 */
	public static void keyRelease(int k)
	{
		keyStates.replace(k, false);
	}
	/**
	 * @param k a billenty� k�dja
	 * @return true, ha az adott billenty� �pp le van nyomva, k�l�nben false.
	 */
	public static boolean isKeyPressed(int k)
	{
		return keyStates.get(k) && !prevKeyStates.get(k);
	}
	

}
