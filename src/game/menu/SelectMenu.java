package game.menu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.List;

import game.GameManager;
import game.Keys;
import game.Screen;

public abstract class SelectMenu extends Menu {
	protected List<String> options;
	protected int selected = 0;
	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt
	 * @param gameManager a j��kot ir�ny�t gameManger referenci�ja.
	 */
	public SelectMenu(GameManager gameManager) {
		super(gameManager);
	}
	/**
	 * A men�t framenk�nt kirajzol� met�dus.
	 */
	public void draw(Graphics screen)
	{	
		Color color;
        
        for (int i = 0; i < options.size(); i++)
        {
        	if (i == selected)
        	{
        		screen.setColor(Color.WHITE);
        		screen.fillRect(200, 300+i*30, 300, 25);
        		screen.setColor(Color.BLACK);
        		color = Color.BLACK;
        	}
        	else
        	{
        		color = Color.WHITE;
        	}
        	Screen.drawText(screen, options.get(i), color, 20, 200, 300+i*30);
        	
        	
        }
	}
	/**
	 * A men�t framenk�nt friss�t� met�dus.
	 */
	public void update()
	{
		if (Keys.isKeyPressed(KeyEvent.VK_W))
			selected--;
		else if (Keys.isKeyPressed(KeyEvent.VK_S))
			selected++;
		
		if (selected >= options.size())
			selected = options.size()-1;
		if (selected < 0)
			selected = 0;
	}

}
