package game.menu;

import java.awt.Graphics;
import java.util.List;

import game.GameManager;

public abstract class Menu {
	protected GameManager gameManager;
	protected List<String> options;
	protected int selected = 0;
	
	public Menu(GameManager gameManager)
	{
		this.gameManager = gameManager;
	}
	
	public abstract void draw(Graphics screen);

	public abstract void update();
}
