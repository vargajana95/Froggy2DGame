package game.menu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;

import game.GameManager;
import game.Keys;
import game.Screen;

public class WinMenu extends Menu {
	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt
	 * @param gameManager a j��kot ir�ny�t gameManger referenci�ja.
	 */
	public WinMenu(GameManager gameManager) {
		super(gameManager);
		
	}
	/**
	 * A men�t framenk�nt kirajzol� met�dus.
	 */
	@Override
	public void draw(Graphics screen) {
		screen.setColor(Color.BLACK);
		screen.fillRect(0, 0, Screen.getSize().width, Screen.getSize().height);
				
		Screen.drawText(screen, "Level: " + gameManager.getLevel(), Color.WHITE, 30, 250, 200);	
	}
	/**
	 * A men�t framenk�nt friss�t� met�dus.
	 */
	@Override
	public void update() {
		if (Keys.isKeyPressed(KeyEvent.VK_ENTER))
		{
			gameManager.setMenu(null);
			gameManager.setPause(false);
			gameManager.newGame();
		}
	}

}
