package game.menu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import game.GameManager;
import game.Keys;
import game.Screen;

public class GameOverMenu extends SelectMenu {
	private boolean isScoreSaved = false;
	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt
	 * @param gameManager a j��kot ir�ny�t gameManger referenci�ja.
	 */
	public GameOverMenu(GameManager gameManager) {
		super(gameManager);
		
		options = new ArrayList<String>();
		options.add("NEW GAME");
		options.add("MAIN MENU");
		options.add("EXIT");
		
	}
	/**
	 * A men�t framenk�nt kirajzol� met�dus.
	 */
	@Override
	public void draw(Graphics screen) {
		screen.setColor(new Color(0, 0, 0, 127));
		screen.fillRect(0, 0, Screen.getSize().width, Screen.getSize().height);
		
		super.draw(screen);
		
		Screen.drawText(screen, "GAME OVER", Color.WHITE, 30, 200, 200);	
		Screen.drawText(screen, "Your score is: " + gameManager.getPlayer().getScore(), Color.WHITE, 20, 200, 240);	
		//t.print(screen);
	}
	/**
	 * A men�t framenk�nt friss�t� met�dus.
	 */
	@Override
	public void update() {
		super.update();
		
		if (!isScoreSaved)
		{
			saveScore();
			isScoreSaved = true;
		}
		if (Keys.isKeyPressed(KeyEvent.VK_ENTER))
		{
			switch (selected)
			{
				case 0: gameManager.setMenu(null);
						gameManager.setPause(false);
						gameManager.newGame();
						break;
				case 1: gameManager.setPause(true);
						gameManager.setMenu(new MainMenu(gameManager));
						break;
				case 2: System.exit(0);
				break;
			}
				
		}
	}
	/**
	 * A j�t�kos el�rt pontjait �rja f�jlba cs�kken� sorrendbe rendezve.
	 */
	public void saveScore()
	{
		class Score implements Comparable<Score> {
			public String name;
			public int score;
			
			public Score(String name, int score)
			{
				this.name = name;
				this.score = score;
			}
			@Override
			public int compareTo(Score s) {
				return -Integer.compare(this.score, s.score);
			}

		}
		
		List<Score> scores = new ArrayList<Score>();
		
		BufferedReader br = null;
		
		String input = JOptionPane.showInputDialog(gameManager, "Please enter your name:");
		
		try {
			File f = new File("HighScores.txt");
			if (!f.exists())
				f.createNewFile();
			br = new BufferedReader(new FileReader(f));
			
			while (true) 
			{
				String line = br.readLine();
				if (line == null) break;
				
				String[] s = line.split(" "); 
				scores.add(new Score(s[0], Integer.parseInt(s[1])));
			}
			br.close();
			scores.add(new Score(input, gameManager.getPlayer().getScore()));
			
			scores.sort(null);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PrintWriter pw = null;
		try {
			 pw = new PrintWriter(new FileWriter("HighScores.txt"));
			 
			 for (Score score : scores)
			 {
				 pw.println(score.name + " " + score.score);
			 }
			 pw.close();
			 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
