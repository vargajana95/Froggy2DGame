package game.menu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import game.GameManager;
import game.Keys;
import game.Screen;

public class PausedMenu extends SelectMenu {
	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt
	 * @param gameManager a j��kot ir�ny�t gameManger referenci�ja.
	 */
	public PausedMenu(GameManager gameManager) {
		super(gameManager);
		
		options = new ArrayList<String>();
		options.add("RESTART");
		options.add("SAVE GAME");
		options.add("MAIN MENU");
		options.add("EXIT");
	}
	/**
	 * A men�t framenk�nt kirajzol� met�dus.
	 */
	@Override
	public void draw(Graphics screen) {
		screen.setColor(new Color(0, 0, 0, 127));
		screen.fillRect(0, 0, Screen.getSize().width, Screen.getSize().height);
		
		super.draw(screen);
		
		Screen.drawText(screen, "PAUSED", Color.WHITE, 30, 200, 200);	
	}
	/**
	 * A men�t framenk�nt friss�t� met�dus.
	 */
	@Override
	public void update() {
		super.update();
		if (Keys.isKeyPressed(KeyEvent.VK_ENTER))
		{
			switch (selected)
			{
				case 0: gameManager.setMenu(null);
						gameManager.setPause(false);
						gameManager.newGame();
						break;
				case 1: gameManager.saveGame();
						gameManager.setPause(false);
						gameManager.setMenu(null);
						break;
				case 2: gameManager.setPause(true);
						gameManager.setMenu(new MainMenu(gameManager));
						break;
				case 3: System.exit(0);
				break;
			}
				
		}
		else if(Keys.isKeyPressed(KeyEvent.VK_ESCAPE))
		{
			gameManager.setPause(false);
			gameManager.setMenu(null);
		}
	}
	
	

}
