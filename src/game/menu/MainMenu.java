package game.menu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import game.GameManager;
import game.Keys;
import game.Screen;

public class MainMenu extends SelectMenu {
	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt
	 * @param gameManager a j��kot ir�ny�t gameManger referenci�ja.
	 */
	public MainMenu(GameManager gameManager)
	{
		super(gameManager);
		options = new ArrayList<String>();
		
		options.add("NEW GAME");
		options.add("HIGH SCORES");
		options.add("LOAD GAME");
		options.add("EXIT");
				
	}
	/**
	 * A men�t framenk�nt friss�t� met�dus.
	 */
	public void update()
	{
		super.update();
		
		if (Keys.isKeyPressed(KeyEvent.VK_ENTER))
		{
			switch (selected)
			{
				case 0: gameManager.setMenu(null);
						gameManager.setPause(false);
						gameManager.newGame();
						break;
				case 1: gameManager.setMenu(new HighScoreMenu(gameManager));
						gameManager.setPause(true);
						break;
				case 2: gameManager.loadGame();
						break;
				case 3: System.exit(0);
						break;
			}
				
		}
	}
	/**
	 * A men�t framenk�nt kirajzol� met�dus.
	 */
	public void draw(Graphics screen)
	{
		screen.setColor(Color.BLACK);
		screen.fillRect(0, 0, Screen.getSize().width, Screen.getSize().height);
		super.draw(screen);		
	}
}
