package game.menu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import game.GameManager;
import game.Keys;
import game.Screen;

public class HighScoreMenu extends Menu {

	/**
	 * A kapott param�terekkel inicializ�lja az oszt�lyt
	 * @param gameManager a j��kot ir�ny�t gameManger referenci�ja.
	 */
	public HighScoreMenu(GameManager gameManager) {
		super(gameManager);
		// TODO Auto-generated constructor stub
	}
	/**
	 * A men�t framenk�nt kirajzol� met�dus.
	 */
	@Override
	public void draw(Graphics screen) {
		screen.setColor(Color.BLACK);
		screen.fillRect(0, 0, Screen.getSize().width, Screen.getSize().height);
		Screen.drawText(screen, "HIGH SCORES", Color.WHITE, 30, 250, 130);
		drawScores(screen);
	}
	/**
	 * A men�t framenk�nt friss�t� met�dus.
	 */
	@Override
	public void update() {
		if(Keys.isKeyPressed(KeyEvent.VK_ESCAPE))
		{
			gameManager.setPause(true);
			gameManager.setMenu(new MainMenu(gameManager));
		}
	}
	/**
	 * El�ssz�r f�jlb�l beolvassa az el�rt pontokat, majd ki�rja �ket a k�perny�re.
	 * @param screen a k�perny�
	 */
	private void drawScores(Graphics screen)
	{
		File f = new File("HighScores.txt");
		if (!f.exists())
			return;
		
		BufferedReader br = null;
		try {
			 br = new BufferedReader(new FileReader(f));
			 
			 for (int i = 1; i <= 10; i++)
			 {
				 String line = br.readLine();
				 Screen.drawText(screen, i+". "+line, Color.WHITE, 20, 250, 150+i*40);
			 }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
