package game;

import javax.swing.JFrame;

public class Game extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private GameManager gameManager;

	/**
	 * A konstruktor l�trehozza a j�t�k ablak�t �s az eg�sz j�t�kot ir�ny�t� gameManager-t
	 */
	public Game()
	{
		setTitle("Froggy");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gameManager = new GameManager();
		setContentPane(gameManager);
		setResizable(false);
		pack();
		setVisible(true);
	}

	public static void main(String[] args) {
		Game game = new Game();
		
		
		game.run();

	}
	
	/**
	 * Ebben a met�dusban fut a j�t�k egy v�gtelen ciklusban.
	 */
	public void run() 
	{	
		long startTime = System.nanoTime();
		//double fps = 1000000000D /30D;
		double elapsed = 0;
		
		long wait;
		
		while(true)
		{
			/*long now = System.nanoTime();
			elapsed += (now-startTime)/fps;
			startTime = now;
			
			while(elapsed >=1)
			{
				
				gameManager.update();
				
				
				//gameManager.draw();
				gameManager.repaint();
				
				elapsed-=1;
			}*/
			
			startTime = System.nanoTime();
			
			gameManager.run();
			
			gameManager.repaint();
			
			
			elapsed = System.nanoTime() - startTime;
			
			wait =(1000/30) -(long) elapsed /1000000; 
			if(wait < 0) wait = 5;
			
			try {
				Thread.sleep(wait);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		
	}
	
	
}
